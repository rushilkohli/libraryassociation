package institute;

import java.util.List;

/**
 *
 * @author rushil
 */
public class Library {
    private final List<Book> books;
    
    Library(List<Book> books) {
        this.books = books;
    }
    
    public List<Book> getTotalBooksInLibrary() {
        return books;
    }
}
