package institute;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author rushil
 */
public class Test {
    public static void main(String[] args) {
        Book b1 = new Book("Learning Java" , "Josh Bloch");
        Book b2 = new Book("Learning JavaScript" , "Bruce Schildt");
        
        List<Book> books = new ArrayList<Book>();
        books.add(b1);
        books.add(b2);
        
        Library library = new Library(books);
        
        List<Book> b3 = library.getTotalBooksInLibrary();
        for(Book b4 : b3) {
            System.out.println("Title : " + b4.getTitle() + " and " + " Author : " + b4.getAuthor());
        }
    }
}
